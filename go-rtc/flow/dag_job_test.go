package flow

import (
	"context"
	"fmt"
	"testing"
	"time"
)

func TestDAGJob(t *testing.T) {
	job := NewDAGJob()

	Getup := NewNode(func() {
		fmt.Println("起床")
	})
	//构建节点
	UnderpantsNode := NewNode(func() {
		fmt.Println("内裤")
		time.Sleep(1 * time.Second)
	})
	SocksNode := NewNode(func() {
		fmt.Println("袜子")
		time.Sleep(4 * time.Second)
	})
	ShirtNode := NewNode(func() {
		fmt.Println("衬衫")
		time.Sleep(3 * time.Second)
	})
	WatchNode := NewNode(func() {
		fmt.Println("手表")
		time.Sleep(2 * time.Second)
	})
	TrousersNode := NewNode(func() {
		fmt.Println("裤子")
		time.Sleep(3 * time.Second)
	})
	ShoesNode := NewNode(func() {
		fmt.Println("鞋子")
		time.Sleep(5 * time.Second)
	})
	CoatNode := NewNode(func() {
		fmt.Println("外套")
		time.Sleep(3 * time.Second)
	})

	//构建节点之间的关系
	job.AddStartNode(Getup)
	job.AddEdge(Getup, UnderpantsNode)
	job.AddEdge(Getup, SocksNode)
	job.AddEdge(Getup, ShirtNode)
	job.AddEdge(Getup, WatchNode)

	job.AddEdge(UnderpantsNode, TrousersNode)
	job.AddEdge(TrousersNode, ShoesNode)
	job.AddEdge(SocksNode, ShoesNode)
	job.AddEdge(ShirtNode, CoatNode)
	job.AddEdge(WatchNode, CoatNode)

	job.ConnectToEnd(ShoesNode)
	job.ConnectToEnd(CoatNode)

	job.StartWithContext(context.Background())
	job.WaitDone()
}
