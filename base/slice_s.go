package base

type RequestUnion struct {
	word string
}

func (ru *RequestUnion) SetInner(val string) {
	ru.word = val
}

type VecScanResponse struct {
	ColBatch []VecValue
}

// columnar value batch: single column set
type VecValue struct {
	ColId   uint32
	ColType int32
	Values  [][]uint32
}

func (vsr *VecScanResponse) Combine(oth VecScanResponse) error {
	if vsr != nil {
		//vsr.ColBatch = append(vsr.ColBatch, oth.ColBatch...)
		vsrColNum := len(vsr.ColBatch)
		othColNum := len(oth.ColBatch)
		maxColNum := vsrColNum
		if maxColNum < othColNum {
			maxColNum = othColNum
		}
		for i := 0; i < maxColNum; i++ {
			if maxColNum == vsrColNum {
				if vsr.ColBatch[i].ColId == oth.ColBatch[i].ColId {
					vsr.combineVecValue(oth.ColBatch, i)
				} else {

				}
			} else {

			}
		}
		//vsr.IntentRows = append(vsr.IntentRows, otherVSR.IntentRows...)
		//if err := vsr.ResponseHeader.combine(otherVSR.Header()); err != nil {
		//	return err
		//}
	}
	return nil
}

func (vsr *VecScanResponse) combineVecValue(colBatch []VecValue, i int) {
	for _, v := range colBatch[i].Values {
		vsr.ColBatch[i].Values = append(vsr.ColBatch[i].Values, v)
	}
}
