module gitee.com/golang/redis

go 1.17

require (
	github.com/RedisTimeSeries/redistimeseries-go v1.4.4
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/rs/xid v1.3.0
)

require (
	github.com/gomodule/redigo v1.8.2 // indirect
	github.com/google/go-cmp v0.5.5 // indirect
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.17.0 // indirect
)
