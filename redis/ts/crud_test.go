package ts

import (
	"fmt"
	redistimeseries "github.com/RedisTimeSeries/redistimeseries-go"
	"strconv"
	"testing"
	"time"
)

const (
	//URL = "localhost:6379"
	URL = "node9.cloud.com:9019"
)

// 时序数据库模块测试
func TestCase01(t *testing.T) {
	// Connect to localhost with no password
	var client = redistimeseries.NewClient(URL, "nohelp", nil)
	var keyname = "mytest"
	_, haveit := client.Info(keyname)
	if haveit != nil {
		client.CreateKeyWithOptions(keyname, redistimeseries.DefaultCreateOptions)
		client.CreateKeyWithOptions(keyname+"_avg", redistimeseries.DefaultCreateOptions)
		client.CreateRule(keyname, redistimeseries.AvgAggregation, 60, keyname+"_avg")
	}
	// Add sample with timestamp from server time and value 100
	// TS.ADD mytest * 100
	_, err := client.AddAutoTs(keyname, 100)
	if err != nil {
		fmt.Println("Error:", err)
	}
}

func TestCase02(t *testing.T) {
	// Connect to localhost with no password
	var client = redistimeseries.NewClient(URL, "nohelp", nil)
	endpoint := "temperature"
	device := "device_lynn_"
	var keynames = make([]string, 1)
	for i := 0; i < 1; i++ {
		labels := map[string]string{
			"name":   device + strconv.Itoa(i+1),
			"tagk_1": "tagv_" + strconv.Itoa(i+1),
		}

		keyname := endpoint + ":" + device
		keynames = append(keynames, keyname)
		options := redistimeseries.CreateOptions{
			Uncompressed:    false,
			RetentionMSecs:  0,
			Labels:          labels,
			ChunkSize:       0,
			DuplicatePolicy: "",
		}
		_, haveit := client.Info(keyname)
		if haveit != nil {
			client.CreateKeyWithOptions(keyname, options)
		} else {
			fmt.Printf("Key: %s has been already in the db\n", keyname)
		}

		for n := 0; n < 10; n++ {
			fmt.Println(n)
			_, err := client.AddAutoTs(keyname, float64(time.Now().Nanosecond()/1000))
			if err != nil {
				fmt.Errorf("Error: %v", err)
			}
			time.Sleep(time.Duration(10))
		}
	}

	// 如何查询tags
	ranges, err := client.MultiGetWithOptions(redistimeseries.MultiGetOptions{
		WithLabels: true,
	}, "tagk_1=tagv_1")

	if err != nil {
		fmt.Errorf("Error: %v", err)
	}
	fmt.Println("--------------MultiGetWithOptions----------------")
	for _, rng := range ranges {
		fmt.Println(rng)
	}

	for _, keyname := range keynames {
		to := time.Now().UnixMicro()
		dps, err := client.RangeWithOptions(keyname, 0, to, redistimeseries.DefaultRangeOptions)
		if err != nil {
			fmt.Errorf("Error: %v", err)
		}
		fmt.Printf("--------------RangeWithOptions %s {%d, %d}----------------\n", keyname, 0, to)
		for _, dp := range dps {
			fmt.Println(dp)
		}
	}

	// 聚合
	//client.MultiRangeWithOptions(0, to, redistimeseries.MultiRangeOptions{
	//	WithLabels: true,
	//}, "tagk_1=tagv_1")
}
