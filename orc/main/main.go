package main

import (
	"github.com/scritchley/orc"
	"log"
)

const (
	GOPATH = "/go"
	GOMOD  = GOPATH + "/pkg/mod"
	GITHUB = GOMOD + "/github.com"

	_ = GITHUB
)

func main() {
	r, err := orc.Open(GITHUB + "/scritchley/orc@v0.0.0-20201124122313-cba38e582ef9/examples/demo-12-zlib.orc")
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()

	// Create a new Cursor reading the provided columns.
	c := r.Select("_col0", "_col1", "_col2")

	// Iterate over each stripe in the file.
	for c.Stripes() {
		// Iterate over each row in the stripe.
		for c.Next() {
			// Retrieve a slice of interface values for the current row.
			log.Println(c.Row())
		}
	}

	if err := c.Err(); err != nil {
		log.Fatal(err)
	}
}
