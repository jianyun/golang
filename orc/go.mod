module gitee.com/jianyun/golang/orc

go 1.16

require (
	github.com/golang/protobuf v1.5.1 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/scritchley/orc v0.0.0-20201124122313-cba38e582ef9
)
