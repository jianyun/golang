module gitee.com/jianyun/golang/oceanbase

go 1.12

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/panjf2000/ants/v2 v2.4.6
)
