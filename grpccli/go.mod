module gitee.com/jianyun/golang/grpccli

go 1.12

require (
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.25.0
)
