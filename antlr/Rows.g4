grammar Rows;

// members动作可以将代码注入到生成的语法分析器类中, 使之称为该类的成员
@parser::members { 
    // 在生成的RowsParser中添加一些成员
    int col;
    public RowsParser(TokenStream input, int col) { // 自定义的构造器
        this(input);
        this.col = col;
    }
}

file: (row NL)+ ; 

// 在row规则中的动作访问了$i, 它是一个使用locals子句定义的局部变量
row
locals [int i=0]
    : ( STUFF 
        {
            $i++;
            if ($i == col) System.out.println($STUFF.text);
        }
     )+
     ;

TAB : '\t' -> skip ; // 匹配但是不将其传递给语法分析器
NL : '\r'? '\n' ; // 匹配并将其传递给语法分析器
STUFF: ~[\t\r\n]+ ; // 匹配除tab符和换行符之外的任何字符


// export CLASSPATH="$CLASSPATH:.:/usr/local/antlr/antlr-4.10.1-complete.jar"
// antlr4 -Dlanguage=Java -o java/rows Rows.g4


// antlr4 -Dlanguage=Java -no-listener -visitor -o java/rows Rows.g4