module calculator

go 1.17

require parser v0.0.0

require github.com/antlr/antlr4/runtime/Go/antlr v0.0.0-20220626175859-9abda183db8e

replace parser => ../parser
