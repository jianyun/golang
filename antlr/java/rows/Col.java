import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.FileInputStream;
import java.io.InputStream;


public class Col {
    
    public static void main(String[] args) throws Exception {
        String inputFile = null;
        InputStream is = System.in;
        int col = 0;
        if (args.length > 1) {
            inputFile = args[0];
            col = Integer.valueOf(args[1]);
        }
        
        if (inputFile != null) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        RowsLexer lexer = new RowsLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        // 传递列好作为参数
        RowsParser parser = new RowsParser(tokens, col);
        // 不需要浪费时间简历语法分析树
        parser.setBuildParseTree(false);
        // 开始语法分析
        parser.file();
    }
}
