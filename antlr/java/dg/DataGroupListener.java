// Generated from DataGroup.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link DataGroupParser}.
 */
public interface DataGroupListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link DataGroupParser#file}.
	 * @param ctx the parse tree
	 */
	void enterFile(DataGroupParser.FileContext ctx);
	/**
	 * Exit a parse tree produced by {@link DataGroupParser#file}.
	 * @param ctx the parse tree
	 */
	void exitFile(DataGroupParser.FileContext ctx);
	/**
	 * Enter a parse tree produced by {@link DataGroupParser#group}.
	 * @param ctx the parse tree
	 */
	void enterGroup(DataGroupParser.GroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link DataGroupParser#group}.
	 * @param ctx the parse tree
	 */
	void exitGroup(DataGroupParser.GroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link DataGroupParser#sequence}.
	 * @param ctx the parse tree
	 */
	void enterSequence(DataGroupParser.SequenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link DataGroupParser#sequence}.
	 * @param ctx the parse tree
	 */
	void exitSequence(DataGroupParser.SequenceContext ctx);
}